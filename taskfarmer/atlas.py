import math
import sys
import os.path

from . import task

try:
    import ROOT
except:
    ROOT=None

def tasklist_simple(filelist): 
    """
    Convert a filelist to tasks by assigning a file per task.
    """
    for infile in filelist:
        task={
            f'inputFile':infile
        }
        yield task

def tasklist_nevents(filelist,maxEventsPerJob,tree='CollectionTree',maxEventsOpt='maxEvents'):
    """
    Convert a filelist to tasks by splitting a file into tasks with
    a maximum of `maxEventsPerJob` events per job.

    The event count is determined by opening each input file and
    looking at tree `tree`.

    Parameters
    ----------
    filelist : list of str
        List of input files.
    maxEventsPerJob : int
        Maximum number of events per job.
    tree : str
        Name of the tree with events
    maxEventsOpt : str
        Name of the option containing maximum number of events in task.
    """
    # Count number of events
    for infile in filelist:
        taskbase={
            f'inputFile':infile
        }

        # Study the input file
        fh=ROOT.TFile.Open(infile)
        nevents=fh.Get(tree).GetEntries()

        # Split
        for i in range(int(math.ceil(nevents/maxEventsPerJob))):
            task=taskbase.copy()
            task['skipEvents']=int(i*maxEventsPerJob)
            task[maxEventsOpt]=int(  maxEventsPerJob)
            yield task

def tasklist_nfiles(filelist,maxFilesPerJob):
    """
    Convert a filelist to tasks by assining up to `maxFilesPerJob` files
    per task.
    """
    ntasks=math.ceil(len(filelist)/maxFilesPerJob)
    for taskid in range(ntasks):
        f=int( taskid   *maxFilesPerJob)
        t=int((taskid+1)*maxFilesPerJob)
        task={
            f'inputFile':' '.join(filelist[f:t])
        }
        yield task

class TransformTaskList(task.ListTaskList):
    """
    Run an ATLAS transform on input ROOT files.

    See the :code:`__init__` function on details how to configure this tasklist 
    handler. A simple example for running no pileup digitization is below.

    .. code-block:: ini

        [digi]
        TaskList = taskfarmer.atlas.TransformTaskList
        transform = Reco_tf.py
        input = HITS
        output = RDO
        autoConfiguration = everything
        digiSteeringConf = StandardInTimeOnlyTruth
        conditionsTag = default:OFLCOND-MC16-SDR-RUN2-06
        geometryVersion = default:ATLAS-R2-2016-01-00-01
        postInclude = default:PyJobTransforms/UseFrontier.py
        preInclude = HITtoRDO:Campaigns/MC16NoPileUp.py
        preExec = all:from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinGeantTruth.set_Value_and_Lock(False);' 'HITtoRDO:from Digitization.DigitizationFlags import digitizationFlags; digitizationFlags.OldBeamSpotZSize = 42

    The :code:`TransformTaskList` supports splitting each input file into
    multiple tasks, based on a maximum number of events. However, when
    practical, it is recommeded to use AthenaMP for parallelizing event
    processing. This has a reduced memory footprint. AthenaMP can enabled by
    including the following in your tasklist handler defintion.

    .. code-block:: ini

        athenaopt = all:--nprocs=64

    or by setting the :code:`ATHENA_PROC_NUMBER` environmental variable.

    The transform output is stored in the current working directory. It is then
    copied to the `workdir` using rsync. This two stage process is required due
    to how AthenaMP determines its temporary outputs. The implication is that
    the runner needs to run the command using bash.
    """
    def __init__(self, path, workdir, transform, input, output, maxEventsPerJob=None, maxFilesPerJob=None, **kwargs):
        """
        The `kwargs` are interpreted as arguments to the transform command. For
        example, having an kwarg of
        :code:`kwargs['postInclude']="HITtoRDO:Campaigns/MC16NoPileUp.py"`
        translates into a transform argument of
        :code:`--postInclude='HITtoRDO:Campaigns/MC16NoPileUp.py'`. Note the
        automatic wrapping of the value string inside singlue quotes. These are
        automatically by the added by this tasklist handler.

        Input files can be split (using :code:`maxEventsPerJob`) into multiple
        tasks or merged into a single task (using :code:`maxFilesPerJob`). Only
        one of these two options can be used at a time. If both are specified,
        then the :code:`maxEventsPerJob` option is used.

        Parameters
        ----------
        path : str
            Path to tasklist
        workdir : str
            Path to work directory
        transform : str
            Name of transform (ie: :code:`Sim_tf.py`)
        input : str
            Type of input file (ie: :code:`EVNT`)
        output : str
            Type of output file (ie: :code:`HITS`)
        maxEventsPerJob : str, optional
            Maximum number of events per task
        maxFilesPerJob : str, optional
            Maximum number of files per task.
        kwargs
            Arguments passed to athena as :code:`--key='value'`.
        """
        super().__init__(path, workdir)

        if maxEventsPerJob is not None:
            maxEventsPerJob=float(maxEventsPerJob)
        if maxFilesPerJob is not None:
            maxFilesPerJob =float(maxFilesPerJob )

        # Input/output configuration
        self.input=input
        self.output=output
        self.outdir=os.path.realpath(f'{workdir}/output')
        self.lock()
        if not os.path.exists(self.outdir):
            os.mkdir(self.outdir)
        self.unlock()

        # Build up command string
        self.cmd = []
        self.cmd.append(transform)
        for opt,val in kwargs.items():
            self.cmd.append(f"--{opt} '{val}'")

        # Build a list of tasks
        filelist=list(self.tasks)
        tasklistgen=None
        if maxEventsPerJob is None and maxFilesPerJob is None:
            tasklistgen=tasklist_simple (filelist)
        if maxEventsPerJob is not None:
            tasklistgen=tasklist_nevents(filelist, maxEventsPerJob, tree='CollectionTree')
        if maxFilesPerJob is not None:
            tasklistgen=tasklist_nfiles (filelist, maxFilesPerJob)

        self.tasks=[]
        for taskid,task in enumerate(tasklistgen):
            # Inputs and outputs
            outfile=f'{self.output}.{taskid}.root'
            task[f'input{input}File'       ]=task.pop('inputFile')
            task[f'output{self.output}File']=outfile
            self.tasks.append(self.buildcommand(**task))

    def buildcommand(self,**kwargs):
        """ Build task command using hander configuration and per-task settings in `kwargs`. """
        # Build the transform command
        cmd=self.cmd.copy()
        cmd+=list(map(lambda opt: f'--{opt[0]} {opt[1]}', kwargs.items()))

        # Copy the result on success
        cmd.append('&&')
        cmd.append('rsync')
        cmd.append('-avr')
        cmd.append(kwargs[f'output{self.output}File'])
        cmd.append(self.outdir)

        return ' '.join(cmd)

class AthenaTaskList(task.ListTaskList):
    """
    Run an athena job on input ROOT files.

    See the :code:`__init__` function on details how to configure this tasklist 
    handler. A simple example for running no pileup digitization is below.

    .. code-block: ini

        [valid]
        TaskList = taskfarmer.atlas.AthenaTaskList
        jobOptions = InDetPhysValMonitoring/InDetPhysValMonitoring_topOptions.py
        output = M_output.root
        command = from InDetPhysValMonitoring.InDetPhysValJobProperties import InDetPhysValFlags; InDetPhysValFlags.doValidateTruthToRecoNtuple.set_Value_and_Lock(True)

    The job options need to use the built-in athena support for input files
    (ie: :code:`--filesInput`).

    The :code:`AthenaTaskList` supports splitting each input file into multiple
    tasks, based on a maximum number of events. However, when practical, it is
    recommeded to use AthenaMP for parallelizing event processing. This has a
    reduced memory footprint. AthenaMP can enabled by including the following in
    your tasklist handler defintion.

    .. code-block:: ini

        nprocs = 64

    or by setting the :code:`ATHENA_PROC_NUMBER` environmental variable.

    The output file name is set as the :code:`output` setting. The handler looks
    for it in the current working directory and then copies it to the `workdir`
    using rsync. This two stage process is required due to how AthenaMP
    determines its temporary outputs. The implication is that the runner needs
    to run the command using bash.
    """
    def __init__(self, path, workdir, jobOptions, output, maxEventsPerJob=None, maxFilesPerJob=None, **kwargs):
        """
        The `kwargs` are interpreted as arguments the the athena command. For
        :code:`kwargs['postInclude']="HITtoRDO:Campaigns/MC16NoPileUp.py"`
        translates into an athena argument of
        :code:`--postInclude='HITtoRDO:Campaigns/MC16NoPileUp.py'`. Note the
        automatic wrapping of the value string inside singlue quotes. These are
        automatically added by this tasklist handler.

        Input files can be split (using :code:`maxEventsPerJob`) into multiple
        tasks or merged into a single task (using :code:`maxFilesPerJob`). Only
        one of these two options can be used at a time. If both are specified,
        then the :code:`maxEventsPerJob` option is used.

        Parameters
        ----------
        path : str
            Path to tasklist.
        workdir : str
            Path to work directory.
        jobOptions : str
            Name of jobOptions file to execute.
        output : str
            Expected name of output file.
        maxEventsPerJob : str, optional
            Maximum number of events per task.
        maxFilesPerJob : str, optional
            Maximum number of files per task.
        kwargs
            Arguments passed to athena as :code:`--key='value'`.
        """
        super().__init__(path, workdir)

        if maxEventsPerJob is not None:
            maxEventsPerJob=float(maxEventsPerJob)
        if maxFilesPerJob is not None:
            maxFilesPerJob =float(maxFilesPerJob )

        # Input/output configuration
        self.outdir=os.path.realpath(f'{workdir}/output')
        self.lock()
        if not os.path.exists(self.outdir):
            os.mkdir(self.outdir)
        self.unlock()

        # Save arguments
        self.args = kwargs
        self.jobOptions = jobOptions
        self.output = output

        # Build a list of tasks
        filelist=list(self.tasks)
        tasklistgen=None
        if maxEventsPerJob is None and maxFilesPerJob is None:
            tasklistgen=tasklist_simple (filelist)
        if maxEventsPerJob is not None:
            tasklistgen=tasklist_nevents(filelist, maxEventsPerJob, tree='CollectionTree', maxEventsOpt='evtMax')
        if maxFilesPerJob is not None:
            tasklistgen=tasklist_nfiles (filelist, maxFilesPerJob)

        self.tasks=[]
        for taskid,task in enumerate(tasklistgen):
            # Inputs and outputs
            task[f'filesInput']=task.pop('inputFile')
            self.tasks.append(self.buildcommand(taskid,**task))

    def buildcommand(self,taskid,**kwargs):
        """ Build task command using hander configuration and per-task settings in :code:`kwargs`

        The :code:`taskid` value is used to determine the unique output filename for the given
        task as :code:`{outdir}/{output}.{taskid}`.
        """
        # Build the athena command
        cmd=['athena.py']
        for opt,val in self.args.items():
            cmd.append(f"--{opt} '{val}'")
        cmd+=list(map(lambda opt: f'--{opt[0]} {opt[1]}', kwargs.items()))
        cmd.append(self.jobOptions)

        # Copy the result on success
        cmd.append('&&')
        cmd.append('rsync')
        cmd.append('-avr')
        cmd.append(self.output)
        cmd.append(f'{self.outdir}/{self.output}.{taskid}')

        return ' '.join(cmd)
