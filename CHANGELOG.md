v0.2.0 (upcoming)
  - Add: The `atlas` tasklist handlers now aggregate files via `maxFilesPerJob`.
  - Fix: Runners now use `wait` to ensure that a process is completely done.

v0.1.0
  - New Feature: `atlas` module with runners for athena and transforms.
  - New Feature: Sphinx based documentation. Build with `setup.py build_sphinx`.

v0.0.1
  - Initial release.
