Examples
========

Simple Example
--------------

Included in the package is a small test file that you can use as an example. Try
running

.. code-block:: sh

    pytaskfarmer.py mywork.tasks

That will give you a sense of how the thing works. Feel free to kill it and
restart it if you wish.

SLURM Example (Array Jobs)
--------------------------

Example of a batch job for using PyTaskFarmer with SLURM is below. It
demonstrates how to correctly handle cleanup.

.. literalinclude:: ../test/slurm_test.sh
    :language: sh

To run using array jobs:

.. code-block:: sh

    sbatch slurm_test.sh mywork.tasks

SLURM Example (Multi-Node Jobs)
-------------------------------

Example of a batch job for using PyTaskFarmer with a SLURM multi-node job is
below. It demonstrates how to correctly handle cleanup and launch PyTaskFarmer
on multiple nodes using srun.

.. literalinclude:: ../test/srun_test.sh
    :language: sh

To run by requesting multiple nodes at the same time (srun):

.. code-block:: sh

    sbatch srun_test.sh mywork.taskss
