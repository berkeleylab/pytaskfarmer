TaskList Handlers
=================

TaskList Handlers are responsible for parsing the contents of a tasklist into
a set of commands to execute (tasks). This reduces the amount of clutter in each
task definition and common tasks (ie: athena job options) reusable across
tasklists.

The desired tasklist handler is selected using the :code:`--tasklist` option to
the PyTaskFarmer program.

The :code:`ListTaskList` handler is always available under the name
:code:`default`. See :ref:`provided-tasklist-handlers` for the list of tasklists
handlers shipped with PyTaskFarmer.

Defining TaskList Handlers
--------------------------
TaskList Handler definitions are loaded from `pytaskfarmer/tasklists.d` and the
current working directory. All files ending in `.ini` are loaded and are
expected to be the INI format.

The following scheme is expected:

.. code-block:: ini

    [tasklisthandlername]
    TaskList = tasklist.python.class
    Arg0 = value0
    Arg1 = value1

The extra arguments are passed to the `TaskList` constructor as keyword
arguments.

.. _provided-tasklist-handlers:

Provided TaskList Handlers
--------------------------

All TaskList Hander contructions take :code:`path` and :code:`workdir` as the
two positional arguments. They are automatically set by the PyTaskFarmer program
and should not be specified by the user.

Generic Handlers
^^^^^^^^^^^^^^^^

.. autoclass:: taskfarmer.task.ListTaskList
    :members: __init__

ATLAS Handlers
^^^^^^^^^^^^^^

.. autoclass:: taskfarmer.atlas.TransformTaskList
    :members: __init__

.. autoclass:: taskfarmer.atlas.AthenaTaskList
    :members: __init__
