TODO
====
Lost of possible future work to futher improve PyTaskFarmer.

- At the moment, if the original process file is significantly modified (item
  added and removed) or contains duplicates, in some cases the process IDs may
  not be unique. Of course, the output can be re-directed by the user to a log
  file with a more appropriate name, so the log files created by the farmer may
  be dummy. If `PROC_NUMBER` is important to your workflow, then please either
  submit additional farmers for new lists of processes or add a unique (short as
  you like) comment to the end of the command to make the items distinguishable.

- It would be nice to add some monitoring hooks so that we can watch what users
  are doing with this script.

- Longer-term, it would be interesting to try to keep all tasks that need to be
  finished in an sqlite file, including a state (to process, running, finished,
  failed). Adding an integer identifier would solve the above problem and give
  us a free way to add jobs mid-way through a run.

- Storing all configuration inside `workdir` to reduce the overhead in
  restarting PyTaskFarmer. Ideally to restart, one would just have to specify
  the workdir. The tasklist handler and runner should be picked up from it.
