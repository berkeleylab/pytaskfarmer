Runners
=======

Runners define the execution environment in which the tasks are execute. They 
can be also used globally across multiple tasklists. This reduces the amount of
clutter in each task definition and makes tasklists portable across multiple
environments.

The desired runner is selected using the :code:`--runner` option to the
PyTaskFarmer program.

The :code:`BasicRunner` is always available under the name :code:`default`. See
:ref:`provided-runners` for the list of runners shipped with PyTaskFarmer.

Defining Runners
----------------
Custom runners can be defined inside the `~/.pytaskfarmer/runners.d` directory
or the current working directory as INI files. All files ending in `.ini` are
loaded. There can be multiple runners defined in a single file.

The format of a single runner definition is

.. code-block:: ini

    [runnername]
    Runner = runner.python.class
    Arg0 = value0
    Arg1 = value1

where `runnername` is the name of the runner and `Runner` is the Python class
(along with package and module) of the implementation. The remaining key:value
pairs are passed to the :code:`runner.python.class` constructor as keyword
arguments.

.. _provided-runners:

Provided Runners
-----------------
.. autoclass:: taskfarmer.runners.BasicRunner
    :members: __init__

.. autoclass:: taskfarmer.runners.ShifterRunner
    :members: __init__